"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class okok extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  okok.init(
    {
      name: DataTypes.STRING,
      username: DataTypes.STRING,
      age: DataTypes.NUMBER,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "okok",
    }
  );
  return okok;
};
