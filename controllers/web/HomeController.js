const { User } = require("../../models");
const bcrypt = require("bcrypt");

class HomeController {
  dashboard = (req, res) => {
    User.findAll().then((users) => {
      res.render("dashboard", {
        content: "./pages/dashboard",
        title: "Dashboard",
        users,
      });
    });
  };

  users = (req, res) => {
    User.findAll().then((users) => {
      res.render("dashboard", {
        content: "./pages/users",
        title: "Users",
        users,
      });
    });
  };

  addUser = (req, res) => {
    res.render("dashboard", {
      content: "./pages/add-user",
      title: "Add user",
    });
  };

  saveUser = async (req, res) => {
    const salt = await bcrypt.genSalt(10);

    User.create({
      name: req.body.name,
      username: req.body.username,
      age: req.body.age,
      password: await bcrypt.hash(req.body.password, salt),
    })
      .then(() => {
        res.redirect("/dashboard");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  editUser = (req, res) => {
    const id = req.params.id;

    User.findOne({
      where: { id },
    }).then((user) => {
      res.render("dashboard", {
        content: "./pages/edit-user",
        title: "Users",
        user,
      });
    });
  };

  updateUser = (req, res) => {
    User.update(
      {
        name: req.body.name,
        username: req.body.username,
        age: req.body.age,
        password: req.body.password,
      },
      { where: { id: req.params.id } }
    )
      .then(() => {
        res.redirect("/users");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  deleteUser = (req, res) => {
    User.destroy({
      where: { id: req.params.id },
    }).then(() => {
      res.redirect("/users");
    });
  };
}

module.exports = HomeController;
