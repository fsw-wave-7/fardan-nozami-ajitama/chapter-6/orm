const { User } = require("../../models");
const bcrypt = require("bcrypt");

class AuthController {
  login = (req, res) => {
    res.render("login", { content: "./pages/login", title: "Halaman Login" });
  };

  doLogin = async (req, res) => {
    const body = req.body;

    if (!(body.username && body.password)) {
      return res.status(400).send({ error: "Data not formatted properly" });
    }
    User.findOne({
      where: { username: body.username },
    }).then((user) => {
      bcrypt
        .compare(body.password, user.password, (err, data) => {
          if (err) throw err;

          if (data) {
            res.cookie("loginData", JSON.stringify(user));
            res.redirect("/dashboard");
          } else {
            return res.status(401).json({ msg: "invalid credential" });
          }
        })
        .catch((err) => {
          return res.status(401).json({ msg: "invalid credential" });
        });
    });
  };

  logout = (req, res) => {
    res.clearCookie("loginData");
    res.redirect("/login");
  };
}

module.exports = AuthController;
