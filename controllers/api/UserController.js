const { User } = require("../../models");
const { Op } = require("sequelize");
const { query } = require("express");

class UserController {
  getUser = (req, res) => {
    const name = req.query.name;
    User.findAll().then((user) => {
      res.result(user);
    });
  };
  getDetailUser = (req, res) => {
    User.findOne({
      where: { id: req.params.id },
    }).then((user) => {
      res.result(user);
    });
  };
  insertUser = (req, res) => {
    User.create({
      name: req.body.name,
      username: req.body.username,
      age: req.body.age,
      password: req.body.password,
    })
      .then((user) => {
        res.result(user);
      })
      .catch((err) => {
        res.status(422).json("can't create user");
      });
  };
  updateUser = (req, res) => {
    User.update(
      {
        name: req.body.name,
        username: req.body.username,
        age: req.body.age,
        password: req.body.password,
      },
      { where: { id: req.params.id } }
    )
      .then((user) => {
        User.findOne({ where: { id: req.params.id } }).then((user) => {
          res.result(user);
        });
      })
      .catch((err) => {
        res.status(422).json("can't create user");
      });
  };
  deleteUser = (req, res) => {
    User.destroy({
      where: {
        id: req.params.id,
      },
    }).then(() => {
      res.result({});
    });
  };
}

module.exports = UserController;
