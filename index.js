const express = require("express");
const morgan = require("morgan");
const path = require("path");
const { join } = require("path");
const api = require("./routes/api.js");
const web = require("./routes/web.js");

const app = express();
const port = 3000;

// load static file
app.use(express.static(__dirname + "/public"));

// set view engine
app.set("view engine", "ejs");

// third party Middlewarefor logging
app.use(morgan("dev"));

// load routes
app.use("/api", api);
app.use("/", web);

// internal server error handler Middleware
app.use(function (err, req, res, next) {
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

// 404 handlerMiddleware
app.use(function (req, res, next) {
  res.render(join(__dirname, "./views/404"));
});

// run server
app.listen(port, () => {
  console.log(`Server berhasil dijalankan`);
});
